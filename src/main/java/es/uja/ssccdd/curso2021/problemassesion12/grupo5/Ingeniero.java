/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion12.grupo5;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import es.uja.ssccdd.curso2021.problemassesion12.grupo4.ListenerRestaurante;
import static es.uja.ssccdd.curso2021.problemassesion12.grupo5.Utils.QUEUE_INGENIERO;
import es.uja.ssccdd.curso2021.problemassesion12.grupo5.Utils.CalidadImpresion;
import static es.uja.ssccdd.curso2021.problemassesion12.grupo5.Utils.BROKER_URL;
import static es.uja.ssccdd.curso2021.problemassesion12.grupo5.Utils.QUEUE;
import static es.uja.ssccdd.curso2021.problemassesion12.grupo5.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.problemassesion12.grupo5.Utils.TIEMPO_ESPERA_INGENIERO;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Ingeniero implements Runnable {

    private final int iD;
    private final AtomicInteger contadorIDs;
    private final ArrayList<Integer> modelosConfirmados;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;
    private MessageConsumer consumerAsincrono;
    private ReentrantLock lockDeConfirmados;

    public Ingeniero(int iD, AtomicInteger contadosIDs) {
        this.iD = iD;
        this.contadorIDs = contadosIDs;
        this.modelosConfirmados = new ArrayList<>();
    }

    @Override
    public void run() {

        try {
            before();
            task();
        } catch (Exception e) {
            System.out.println("Ingeniero " + iD + " ha encontrado una excepción" + e.getMessage());
        } finally {
            after();
        }
    }

    public void before() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);

        //Lanzamiento del listener asíncrono
        lockDeConfirmados = new ReentrantLock();
        Destination destinationAsincrono = session.createQueue(QUEUE_INGENIERO + iD);
        consumerAsincrono = session.createConsumer(destinationAsincrono);
        consumerAsincrono.setMessageListener(new ListenerRestaurante(iD, modelosConfirmados, lockDeConfirmados));

    }

    public void after() {
        try {
            if (consumerAsincrono != null) {
                consumerAsincrono.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }

    private void task() throws JMSException {

        boolean interrumpido = false;
        MessageProducer producer = session.createProducer(destination);
        ArrayList<Modelo> modelosImpresos = new ArrayList<>();
        Gson gson = new GsonBuilder().create();

        System.out.println("Ingeniero " + iD + " iniciado.");

        while (!interrumpido) {

            try {

                int idModelo = contadorIDs.getAndIncrement();
                CalidadImpresion calidad = CalidadImpresion.getCalidad(ThreadLocalRandom.current().nextInt(VALOR_GENERACION));
                Modelo modelo = new Modelo(idModelo, calidad, this.iD);
                String mensaje = gson.toJson(modelo);

                TextMessage message = session.createTextMessage(mensaje);
                producer.send(message);

                modelosImpresos.add(modelo);

                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_INGENIERO);
            } catch (InterruptedException e) {
                interrumpido = true;
            }

        }

        producer.close();

        imprimirDatos(modelosImpresos);

    }

    private void imprimirDatos(ArrayList<Modelo> modelos) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\nIngeniero ").append(iD).append(" impresos ").append(modelos.size()).append(" modelos.");

        for (Modelo modelo : modelos) {
            mensaje.append("\n\t\t").append(modelo.toString());
        }
        lockDeConfirmados.lock();
        mensaje.append("\n\tDe los cuales confirmados: ").append(modelosConfirmados.toString());
        lockDeConfirmados.unlock();
        
        mensaje.append("\n");

        System.out.println(mensaje.toString());
    }

}
