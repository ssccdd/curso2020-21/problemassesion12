/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion12.grupo6;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class ListenerAlmacen implements MessageListener {

    private final int iD;
    private final ArrayList<Integer> confirmados;
    private final ReentrantLock excMutua;

    public ListenerAlmacen(int iD, ArrayList<Integer> confirmados, ReentrantLock excMutua) {
        this.iD = iD;
        this.confirmados = confirmados;
        this.excMutua = excMutua;
        System.out.println("Listener del almacén " + iD + " iniciado.");
    }

    @Override
    public void onMessage(Message msg) {

        try {

            int id = Integer.parseInt(((TextMessage) msg).getText());

            excMutua.lock();
            confirmados.add(id);
            excMutua.unlock();

            System.out.println("El listener del almacén " + iD + " ha recibido la confirmación de la dosis: " + id);

        } catch (JMSException ex) {
            Logger.getLogger(ListenerAlmacen.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
