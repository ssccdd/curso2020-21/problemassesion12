/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion12.grupo4;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import es.uja.ssccdd.curso2021.problemassesion12.grupo4.Utils.TipoPlato;
import static es.uja.ssccdd.curso2021.problemassesion12.grupo4.Utils.BROKER_URL;
import static es.uja.ssccdd.curso2021.problemassesion12.grupo4.Utils.QUEUE;
import static es.uja.ssccdd.curso2021.problemassesion12.grupo4.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.problemassesion12.grupo4.Utils.TIEMPO_ESPERA_RESTAURANTE;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
import static es.uja.ssccdd.curso2021.problemassesion12.grupo4.Utils.QUEUE_RESTAURANTE;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Restaurante implements Runnable {

    private final int iD;
    private final AtomicInteger contIdentificadores;
    private final ArrayList<Integer> platosConfirmados;
    private ActiveMQConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;
    private MessageConsumer consumerAsincrono;
    private ReentrantLock lockDeConfirmados;

    public Restaurante(int iD, AtomicInteger contIdentificadores) {
        this.iD = iD;
        this.contIdentificadores = contIdentificadores;
        this.platosConfirmados = new ArrayList<>();
    }

    @Override
    public void run() {

        try {
            before();
            task();
        } catch (Exception e) {
            System.out.println("Restaurante " + iD + " ha encontrado una excepción" + e.getMessage());
        } finally {
            after();
        }
    }

    public void before() throws Exception {
        connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);

        //Lanzamiento del listener asíncrono
        lockDeConfirmados = new ReentrantLock();
        Destination destinationAsincrono = session.createQueue(QUEUE_RESTAURANTE + iD);
        consumerAsincrono = session.createConsumer(destinationAsincrono);
        consumerAsincrono.setMessageListener(new ListenerRestaurante(iD, platosConfirmados, lockDeConfirmados));

    }

    public void after() {
        try {
            if (consumerAsincrono != null) {
                consumerAsincrono.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (Exception ex) {
            // No hacer nada
        }
    }

    private void task() throws JMSException {

        ArrayList<Plato> platos = new ArrayList<>();
        System.out.println("Restaurante " + iD + " ha empezado.");
        MessageProducer producer = session.createProducer(destination);
        boolean terminar = false;
        Gson gson = new GsonBuilder().create();

        while (!terminar) {
            try {

                int idPlato = contIdentificadores.getAndIncrement();
                TipoPlato tipo = TipoPlato.getPlato(ThreadLocalRandom.current().nextInt(VALOR_GENERACION));
                Plato plato = new Plato(idPlato, tipo, this.iD);
                String mensaje = gson.toJson(plato);

                TextMessage message = session.createTextMessage(mensaje);
                producer.send(message);

                platos.add(plato);

                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_RESTAURANTE);

            } catch (InterruptedException ex) {
                terminar = true;
            }
        }
        producer.close();
        imprimirDatos(platos);

    }

    private void imprimirDatos(ArrayList<Plato> platos) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\nRestaurante").append(iD).append(" generados ").append(platos.size()).append(" platos.");

        for (Plato plato : platos) {
            mensaje.append("\n\t\t").append(plato.toString());
        }
        lockDeConfirmados.lock();
        mensaje.append("\n\tDe los cuales confirmados: ").append(platosConfirmados.toString());
        lockDeConfirmados.unlock();
        
        mensaje.append("\n");

        System.out.println(mensaje.toString());
    }

}
