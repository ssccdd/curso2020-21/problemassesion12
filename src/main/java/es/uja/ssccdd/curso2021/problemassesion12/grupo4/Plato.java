/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion12.grupo4;

import es.uja.ssccdd.curso2021.problemassesion12.grupo4.Utils.TipoPlato;
import java.util.Date;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Plato {

    private final int iD;
    private final TipoPlato tipo;
    private final int iDRestaurante;

    public Plato(int iD, TipoPlato tipo, int iDRestaurante) {
        this.iD = iD;
        this.tipo = tipo;
        this.iDRestaurante = iDRestaurante;
    }

    public int getiD() {
        return iD;
    }

    public TipoPlato getTipo() {
        return tipo;
    }

    public int getiDRestaurante() {
        return iDRestaurante;
    }

    /**
     * Devuelve una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    @Override
    public String toString() {
        Date now = new Date();
        return "Plato{" + "iD= " + iD + ", tipo de plato= " + tipo + ", restaurante= " + iDRestaurante + "}";

    }

}
