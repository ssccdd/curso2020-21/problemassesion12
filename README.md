[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 12

Problemas propuestos para la Sesión12 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2020-21.

Los ejercicios son diferentes para cada grupo:

- [Grupo 1](https://gitlab.com/ssccdd/curso2020-21/problemassesion12#grupo-1)
- [Grupo 2](https://gitlab.com/ssccdd/curso2020-21/problemassesion12#grupo-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2020-21/problemassesion12#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2020-21/problemassesion12#grupo-4)
- [Grupo 5](https://gitlab.com/ssccdd/curso2020-21/problemassesion12#grupo-5)
- [Grupo 6](https://gitlab.com/ssccdd/curso2020-21/problemassesion12#grupo-6)

### Grupo 1

### Grupo 2

### Grupo 3

### Grupo 4
En esta sesión vamos a modificar la entrega de la sesión anterior usando las nuevas herramientas disponibles en el guion de esta semana. Los cambios por clase son:
- `Plato`:
	- En plato se añadirá una variable para guardar el identificador del restaurante que generó el plato.
- `Restaurante`:
	- Al empezar la ejecución, lanzará un `Listener` para recibir la confirmación asíncrona de los platos enviados por el restaurante, que estará esperando mensajes en el buzón **QUEUE + ".restaurante" + iDRestaurante**.
	- En vez de mandar un mensaje de texto con el id y el tipo, se mandará un json con el objeto plato completo.
	- Al finalizar, imprimirá lo especificado en el anterior ejercicio más la lista de IDs de platos confirmados.
- `Repartidor`:
	- En vez de recibir el mensaje y generar un plato, el plato estará en el mensaje y solo necesita decodificar el json.
	- Una vez procesado el plato y antes de hacer la espera, el repartidor mandará una confirmación de recepción al buzón del restaurante, el mensaje solo contendrá el ID del plato. (Este mensaje lo atenderá el listener)
- `ListenerRestaurante`:
	- Este listener tendrá como variable de instancia el ID del restaurante que lo creó y un array de IDs de platos confirmados compartido con el restaurante que lo creó.
	- Cada vez que se reciba una confirmación se imprimirá por pantalla qué listener ha recibido la confirmación (ID del restaurante) y el ID del plato confirmado, y guardará el ID en el array.
- Al finalizar, se tienen que quedar todos los Producer/ConsumerMessage y Connection cerrados.

### Grupo 5
En esta sesión vamos a modificar la entrega de la sesión anterior usando las nuevas herramientas disponibles en el guion de esta semana. Los cambios por clase son:
- `Modelo`:
	- En modelo se añadirá una variable para guardar el identificador del ingeniero que generó el modelo.
- `Ingeniero`:
	- Al empezar la ejecución, lanzará un `Listener` para recibir la confirmación asíncrona de los modelos enviados por el ingeniero, que estará esperando mensajes en el buzón **QUEUE + ".ingeniero" + iDIngeniero**.
	- En vez de mandar un mensaje de texto con el id y la calidad, se mandará un json con el objeto modelo completo.
	- Al finalizar, imprimirá lo especificado en el anterior ejercicio más la lista de IDs de modelos confirmados.
- `Maquina Postprocesado`:
	- En vez de recibir el mensaje y generar un modelo, el modelo estará en el mensaje y solo necesita decodificar el json.
	- Una vez procesado el modelo y antes de hacer la espera, la máquina mandará una confirmación de recepción al buzón del ingeniero, el mensaje solo contendrá el ID del modelo. (Este mensaje lo atenderá el listener)
- `ListenerIngeniero`:
	- Este listener tendrá como variable de instancia el ID del ingeniero que lo creó y un array de IDs de modelos confirmados compartido con el ingeniero que lo creó.
	- Cada vez que se reciba una confirmación se imprimirá por pantalla qué listener ha recibido la confirmación (ID del ingeniero) y el ID del modelo confirmado, y guardará el ID en el array.
- Al finalizar, se tienen que quedar todos los Producer/ConsumerMessage y Connection cerrados.

### Grupo 6
En esta sesión vamos a modificar la entrega de la sesión anterior usando las nuevas herramientas disponibles en el guion de esta semana. Los cambios por clase son:
- `DosisVacuna`:
	- En las dosis se añadirá una variable para guardar el identificador del almacén que generó la dosis.
- `Almacén`:
	- Al empezar la ejecución, lanzará un `Listener` para recibir la confirmación asíncrona de las dosis enviadas por el almacén, que estará esperando mensajes en el buzón **QUEUE + ".almacen" + iDalmacen**.
	- En vez de mandar un mensaje de texto con el id y fabricante, se mandará un json con el objeto dosis vacuna completo.
	- Al finalizar, imprimirá lo especificado en el anterior ejercicio más la lista de IDs de dosis confirmadas.
- `Enfermero`:
	- En vez de recibir el mensaje y generar una dosis, la dosis estará en el mensaje y solo necesita decodificar el json.
	- Una vez procesado la dosis y antes de hacer la espera, el enfermero mandará una confirmación de recepción al buzón del almacén, el mensaje solo contendrá el ID de la dosis. (Este mensaje lo atenderá el listener)
- `ListenerAlmacén`:
	- Este listener tendrá como variable de instancia el ID del almacén que lo creó y un array de IDs de dosis confirmadas compartido con el almacén que lo creó.
	- Cada vez que se reciba una confirmación se imprimirá por pantalla qué listener ha recibido la confirmación (ID del ingeniero) y el ID dela dosis confirmada, y guardará el ID en el array.
- Al finalizar, se tienen que quedar todos los Producer/ConsumerMessage y Connection cerrados.
